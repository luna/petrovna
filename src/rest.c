#include <curl/curl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "log.h"
#include "libyasuna-private.h"

struct curl_fetch_st {
    char *payload;
    size_t size;
};

size_t write_func(void *contents, size_t size, size_t nmemb, void *data)
{
    size_t realsize = size * nmemb;
    struct curl_fetch_st *p = (struct curl_fetch_st *)data;
    p->payload = realloc(p->payload, p->size + realsize + 1);

    if(p->payload == NULL)
    {
        log_fatal("Failed to realloc memory for response.");
        free(p->payload);
        return -1;
    }

    memcpy(&(p->payload[p->size]), contents, realsize);
    p->size += realsize;
    p->payload[p->size] = 0;

    return realsize;
}

const char *yn_rest_get_gateway(char *base_url)
{
    // Contact discord GET:/api/gateway
    CURL *curl;
    CURLcode res;

    char request_url[256];
    char *url;
    const char *gw_url;

    json_t *root, *gw_url_val;
    json_error_t error;

    struct curl_fetch_st curl_fetch;
    curl_fetch.payload = 0;
    curl_fetch.size = 0;

    curl_global_init(CURL_GLOBAL_DEFAULT);

    curl = curl_easy_init();
    if (curl)
    {
        sprintf(request_url, "%s/gateway", base_url);
        log_trace("Calling GET:%s", request_url);
        curl_easy_setopt(curl, CURLOPT_URL, request_url);

        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_func);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &curl_fetch);

        curl_easy_getinfo(curl, CURLINFO_EFFECTIVE_URL, &url);
        log_trace("actual url for request: %s", url);

        res = curl_easy_perform(curl);
        if (res != CURLE_OK)
        {
            log_error("curl_easy_perform failed");
            return NULL;
        }

        curl_easy_cleanup(curl);
    }

    log_trace("result: %s", curl_fetch.payload); 

    root = json_loads(curl_fetch.payload, JSON_DECODE_ANY, &error);

    if (root == NULL) {
        // TODO: log error
        log_fatal("root is not object");
        return NULL;
    }

    gw_url_val = json_object_get(root, "url");

    if(gw_url_val == NULL)
    {
        log_fatal("failed to extract .url from gateway endpoint json");
        return NULL;
    }

    gw_url = json_string_value(gw_url_val);

    // remove "wss://" from the url
    gw_url++;
    gw_url++;
    gw_url++;
    gw_url++;
    gw_url++;
    gw_url++;

    log_trace("gateway url from json: '%s'", gw_url);

    return gw_url;
}

void yn_post_message(yn_client *client, char *payload_str, const char *channel_id)
{
    CURL *curl;
    CURLcode res;

    struct curl_fetch_st curl_fetch;
    curl_fetch.payload = 0;
    curl_fetch.size = 0;

    curl_global_init(CURL_GLOBAL_DEFAULT);

    curl = curl_easy_init();
    if (curl)
    {
        char request_url[512];
        sprintf(request_url, "%s/channels/%s/messages",
                client->api_base, channel_id);

        log_trace("Calling POST:%s", request_url);
        curl_easy_setopt(curl, CURLOPT_URL, request_url);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, payload_str);

        struct curl_slist *chunk = NULL;

        char auth_header[256];
        sprintf(auth_header, "Authorization: Bot %s", client->token);
        log_trace(auth_header);
        chunk = curl_slist_append(chunk, auth_header);

        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_func);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &curl_fetch);

        res = curl_easy_perform(curl);
        if (res != CURLE_OK)
        {
            log_error("curl_easy_perform failed");
            return;
        }

        log_trace("Finished calling!");
        curl_easy_cleanup(curl);
    }

    log_trace("size:%d, payload:%s", curl_fetch.size, curl_fetch.payload);
}

void yn_send_message(yn_client* client, const char* channel_id, char* content)
{
    json_t *root = json_object();
    char *json_data;

    json_object_set(root, "content", json_string(content));
    json_data = JSON_DUMPS(root);

    yn_post_message(client, json_data, channel_id);
}
