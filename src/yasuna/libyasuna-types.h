#ifndef _YASUNA_TYPES_
#define _YASUNA_TYPES_

#include <stdbool.h>
#include "libyasuna-enums.h"

// typedef unsigned long long int yn_snowflake_t;
typedef const char* yn_snowflake_t;

typedef struct {
    yn_snowflake_t id;
    const char* username;
    const char* discriminator;
    // avatar hash
    const char* avatar;
    bool bot;
} yn_user_t;

typedef struct {
    bool _init;

    yn_snowflake_t id;
    bool unavailable;
} yn_unavailable_guild_t;

// == channels
// base fields for channels
typedef struct {
    yn_snowflake_t id;
    yn_snowflake_t guild_id;
    char name[256];
    enum yn_channel_type type;
    int position;
} yn_channel_t;

typedef struct {
    yn_channel_t* channel;

    char topic[2048];
    yn_snowflake_t last_message_id;
    bool nsfw;
} yn_text_channel_t;

typedef struct {
    yn_channel_t* channel;
    int bitrate;
} yn_voice_channel_t;

typedef struct {
    yn_channel_t* channel;
    void *extra;
} yn_void_channel_t;

// linked list of channels
typedef struct yn_channel_list_t {
    // generic channel, the extra field can be pointing
    // to a yn_voice_channel_t or a yn_text_channel_t
    yn_void_channel_t channel;
    struct yn_channel_list_t* next;
} yn_channel_list_t;

// == members
typedef struct {
    yn_user_t* user;
    yn_snowflake_t guild_id;
} yn_member_t;

typedef struct yn_member_list_t {
    yn_member_t *member;
    struct yn_member_list_t* next;
} yn_member_list_t;

// "inherits" from yn_unavailable_guild_t by providing
// the same starting fields
typedef struct {
    bool _init;

    yn_snowflake_t id;
    bool unavailable;

    char name[256];
    yn_snowflake_t owner_id;
    int verification_level;

    // TODO: yn_guild_features_t struct?
    char* features;

    bool large;
    yn_member_list_t* members;
    yn_channel_list_t* channels;
} yn_guild_t;

typedef struct {
    yn_snowflake_t id;
    yn_snowflake_t channel_id;
    yn_snowflake_t author_id;

    const char* content;

    const char *tstamp_raw;
    const char *edited_tstamp_raw;

    bool tts;
    bool mention_everyone;

    int nonce;
    bool pinned;

    // TODO: mentions, role_mentions, attachments, embeds, reactions

    enum yn_message_type type;
} yn_message_t;

#endif
