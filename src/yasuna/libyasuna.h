#ifndef _YASUNA_H_
#define _YASUNA_H_

#include <stdbool.h>
#include <stdlib.h>

#include "libyasuna-types.h"

// the public interface for libyasuna. includes
// definitions for all types, public functions, etc

// == client

typedef struct {
    bool set;
    int code;
    char *reason;
} yn_wserr_t;

typedef struct {
    char *api_base;
    char *token;

    // struct yn_clientuser *user;
    yn_wserr_t *ws_err;

    yn_user_t *user;

    // in-memory guild store
    size_t guild_size;
    yn_guild_t *guilds;
    yn_unavailable_guild_t *unavailable_guilds;

    // pointer to the yn_client_private type
    // holding library-specific fields
    void *priv;
} yn_client;

// create a single client instance for future usage
yn_client *yn_client_make(char* api_base, char* token);

// close the client, freeing its allocated resources
int yn_client_close(yn_client*);

int yn_main_loop(yn_client*);

// == event types and event handling
typedef enum {
    YN_EVT_READY,
    YN_EVT_MESSAGE_CREATE,

    // mark for the total amount of events
    YN_EVT_MAX_EVT
} yn_event;

typedef int (*yn_event_handler_t) (yn_client*, void*);
void yn_handler_add(yn_event, yn_event_handler_t, char*);
void yn_event_emit(yn_client*, yn_event, void*);
void yn_event_inspect(yn_event);
void yn_event_remove(yn_event, char*);

// TODO
//yn_channel_t *yn_client_get_channel(yn_client *, int);
//yn_guild_t *yn_client_get_guild(yn_client *, int);

// NOTE: probably redo this one
void yn_send_message(yn_client*, const char*, char*);


// == string representation helpers
size_t yn_message_repr(yn_message_t*, char*, size_t);
size_t yn_user_repr(yn_user_t*, char*, size_t);

// == guild store
yn_guild_t *yn_guild_store_get(yn_client*, yn_snowflake_t);
int yn_guild_store_insert(yn_client*, yn_guild_t*);
int yn_guild_store_del(yn_client*, yn_snowflake_t);

#endif
