#!/bin/bash

# deps.sh - download depedencies

function install_libdill(){
    wget "http://libdill.org/libdill-2.14.tar.gz"
    tar xzf "libdill-2.14.tar.gz"
    cd 'libdill-2.14'

    # compile libdill
    ./configure --enable-tls
    make

    # install it
    sudo make install
}

mkdir -p lib_build && cd lib_build
install_libdill || exit 1
cd ..
