# yasuna

a barebones discord library written in a mix of c and zig,
currently targeting c.

very proof-of-concept, will likely not work with you

## how do

- curl (and headers), your system should have a package
- libdill
  - openssl/libressl 1.1.0 or later(???)
  - libtool
  - automake
- jansson (and headers)

```bash
# building libdill manually
# required for void linux (their builds dont have tls)
./deps.sh

# the reason the copy is required is probably related to
# https://github.com/ziglang/zig/issues/2041 but its unsure.
# way better than it was before, at least.
sudo cp /usr/local/include/libdill.h /path/to/zig/folder/include
```

```bash
zig build install --prefix ~/.local
```

run one of the examples

```bash
echo -n "your token" > token
zig build run-pong
```
