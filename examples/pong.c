#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>

#include "../src/yasuna/libyasuna.h"

yn_client *client;

// this is only a draft of what yasunna could be as a proper lib

int message_create_handler(yn_client* client, void* priv_data)
{
    yn_message_t* message = (yn_message_t*) priv_data;

    printf("beep!!!!!\n");

    char content[300];

    if(strcmp(message->content, "!!ping") == 0)
    {
        yn_user_repr(client->user, content, 300);
        yn_send_message(client, message->channel_id, content);
        return 0;
    }

    return 0;
}


// function to fetch a token from a token file
// borrowed from old src/main.c
char *token_get()
{
    FILE* token_fd = fopen("token", "r");

    if(token_fd == NULL)
    {
        perror("failed to open token file");
        return NULL;
    }

    char* res = calloc(256, sizeof(char));

    if(res == NULL)
    {
        perror("failed to malloc");
        return NULL;
    }

    fread(res, 256, 1, token_fd);
    fclose(token_fd);
    return res;
}


// sigint handler for ctrl-c and properly closing the client
void sigint_handler(int sig) {
    int status;
    printf("closing client from sigint\n");
    status = yn_client_close(client);
    exit(status);
}


int main(int argc, char** argv)
{
    if(signal(SIGINT, sigint_handler) == SIG_ERR)
    {
        perror("failed to register sigint handler");
        return 1;
    }

    char* token = token_get();
    if(token == NULL)
    {
        perror("failed to fetch token file");
        return 1;
    }

    // yn_client_create will query the gateway url from base_url
    // when main_loop gets called
    client = yn_client_make(
        "https://discordapp.com/api/v6", token);

    // you can use yn_client_create_discord for the default
    // base url, whatever that is

    yn_handler_add(YN_EVT_MESSAGE_CREATE, message_create_handler, "msg_create");

    // debug things
    yn_event_inspect(YN_EVT_MESSAGE_CREATE);
    return yn_main_loop(client);
}
